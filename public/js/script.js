var audio = new Audio('js/ring.wav'); // звук сообщения
var regxp_image = new RegExp("(http(s?):/)(/[^/]+)+" + "\.(?:jpg|gif|png)", "g");
var intervalTitle = 0; // Переменная для хранения интервала заголовка
var activeTab = 1; // Переменная для проверка активной вкладки
var host = window.location.origin;

/* SOCKET-IO */
var socket = io();


/* Обработка входящих сообщений */
socket.on('chat message', function(msg) {
    addMsg(msg);
});


/* Получаем кол-во пользователей на сайте */
socket.on('count users', function(msg) {
    $('.users_count').text(msg);
});


/* Отправка стикеров */
socket.on("send sticker", function(sticker){
    addSticker(sticker);
});


/* Серверные сообщения */
socket.on("server message", function(msg) {

    var p = document.createElement("p"); // Элемент для отображения ответа от сервера
    $(p).attr("class", "subtext text-center");

    var row = document.createElement("div");
    $(row).addClass("col-lg-12 col-xs-12 col-md-12 col-sm-12");
    
    var hr = document.createElement("hr");
    $(row).append($(hr)); 

    var a = document.createElement("a");
    $(a).attr("href", "/chat");
    $(a).text("Начать новый чат");
    $(a).attr("class", "center-link");

    if(msg == "wait") {

        $(p).html("<span class='label label-info'>Ждем собеседника</span>");

        $("#messages").append(p);
        disableInterface();

    }

    if(msg == "connect") {

        $(p).html("<span class='label label-success'>Собеседник присоединился</span>");
        audio.play();

        $("#messages").append(p);
        $("#messages").append($(row));   

        enableInterface(); 

    }

    if(msg == "user disconnect") {

        socket.disconnect(); 

        $(p).html("<span class='label label-danger'>Собеседник отключился</span>");
        audio.play();
        
        $("#messages").append($(row));
        $("#messages").append(p);
        $("#messages").append(a);

        disableInterface();

    }

    if(msg == "connection lost") {

        socket.disconnect() 

        $(p).html("<span class='label label-danger'>Соединение потеряно</span>");
        audio.play();

        $("#messages").append($(row));
        $("#messages").append(p);
        $("#messages").append(a);

        disableInterface();

    }       
});


/* Индикатор набора */
socket.on("typing text", function() {
    add_typing_indicator();
});

/* END SOCKET-IO */



/* Служебный функции */

/**
 * Проверка активна ли вкладка с чатом
 * @return 1 - активна; 0 - нет.
 */
var vis = (function() {

    var stateKey, eventKey, keys = {

        hidden: "visibilitychange",
        webkitHidden: "webkitvisibilitychange",
        mozHidden: "mozvisibilitychange",
        msHidden: "msvisibilitychange"

    };

    for (stateKey in keys) {

        if (stateKey in document) {

            eventKey = keys[stateKey];
            break;

        }

    }

    return function(c) {

        if (c) document.addEventListener(eventKey, c);
        return !document[stateKey];

    }

})();

vis(function(){
    activeTab =  vis() ? 1 : 0;
});





if(window.location.pathname == "/chat") {

    socket.emit("start chat", 
                {
                    "myGender": localStorage['myGender'],
                    "chatGender": localStorage['chatGender']
                }
    ); 

    // начинаем чат, если пользователь находится на странице чата

    // window.onbeforeunload = function () {
    //     return "Вы уверены, что хотите закрыть чат?";
    // }

}

if(window.location.pathname == "/") {

    (function(){

        var myGender = $('.myGender');
        var chatGender = $('.chatGender');

        localStorage['myGender'] = $(myGender[0]).val();
        localStorage['chatGender'] = $(chatGender[0]).val();

    })();

    $('.myGender').change(function(){
        localStorage['myGender'] = $(this).val();
    });

    $(".chatGender").change(function(){
        localStorage['chatGender'] = $(this).val();
    });

    socket.emit("count users"); // Получаем число активных пользователей на сайте
}


/* END */


//________________//







/**
 * Функция добавления сообщения чат
 */
function addMsg(msg) {

    var row = document.createElement('div');
    $(row).attr("class", "col-lg-12");

    var col6 = document.createElement('div');
    var span = document.createElement('span');
    var timeBlock = get_block_time();
    

    
    // msg.sender == 0 - отправил текущий пользователь
    // msg.sender == 1 - отправил собеседник
    if(msg.sender == 0) {

        $(col6).attr("class", "col-lg-6 col-md-10 col-sm-10 col-xs-10 msg right pull-right");
        $(timeBlock).addClass("right");
        $(span).attr("class", "pull-right");

    }

    else {

        $(col6).attr("class", "col-lg-6 col-md-10 col-sm-10 col-xs-10 msg left pull-left");
        $(span).attr("class", "pull-left");
        $(timeBlock).addClass("left");

        changeTabTitle(); // Меняем название вкладки, если она не активна
        audio.play();

    }

    
    
    // Проверяем есть ли в сообщении ссылки на изображения.
    // Если есть, то заменяем их на изображения
    if(regxp_image.test(msg.text)) {

        var images = get_array_image(msg.text); // Получем массив тегов image из сообщения
        text = msg.text.replace(regxp_image, ""); // Удаляем ссылки из изображения

        $(span).text(text);

        images.forEach(function(img) {
            $(span).append($(img));
        });

    }


    else {
        $(span).text(msg.text);
    }

    $(col6).append(span);
    $(row).append(col6);
    $(row).append($(timeBlock));

    $("#messages").append(row);
    $("#messages").animate({ scrollTop: $("#messages").prop("scrollHeight")}, 300);

}


/**
 * Функция добавления стикера в диалог чата
 */
function addSticker(sticker) {

    var img = document.createElement("img");
    $(img).attr("src", "images/stickers/" + sticker.sticker_id + ".png");

    var row = document.createElement('div');
    $(row).attr("class", "col-lg-12");

    var col6 = document.createElement('div');
    var timeBlock = get_block_time();

    if(sticker.sender == 0) {

        $(col6).attr("class", "col-lg-6 col-md-6 col-sm-9 col-xs-9 pull-right");
        $(timeBlock).addClass("right");
        $(img).attr("class", "sticker pull-right");

    }

    else {

        $(col6).attr("class", "col-lg-6 col-md-6 col-sm-9 col-xs-9  pull-left");
        $(img).attr("class", "sticker pull-left");
        $(timeBlock).addClass("left");

        changeTabTitle(); // Меняем название вкладки, если она не активна
        audio.play();

    }

    $(col6).append($(img));

    $(row).append(col6);
    $(row).append($(timeBlock));
    $("#messages").append(row);
    $("#messages").animate({ scrollTop: $("#messages").prop("scrollHeight")}, 10);
    
}


/**
 * Парсит текст сообщения заменяя все ссылки на изображения и заменях их на ссылки с изображением 
 */ 
function get_array_image(text) {

    var images = [];
    var links = [];
    var result = text.match(regxp_image);

    for(var i = 0; i < result.length; i+=2) {
        var url = result[i];

        if($.inArray(url, links) == -1) {
            links.push(url);

            var img = get_image_from_url(url);
            images.push(img);
        }
    }

    return images;

}


/**
 * Функция получения тега img с указанным url
 */
function get_image_from_url(url) {

    var a = document.createElement("a");
    $(a).attr("href", url);

    var img = document.createElement("img");
    $(img).attr("src", url);


    $(a).append($(img));
    return img;

}


/**
 * Функция получения html блока с текущим временем
 */
function get_block_time() {

    var timeBlock = document.createElement("div");
    var time = get_current_time();

    $(timeBlock).addClass("time col-lg-12 col-xs-12 col-sm-12 col-md-12");
    $(timeBlock).text(time);

    return $(timeBlock);
}


/**
 * Функция получения текущего времени
 */
function get_current_time() {
    var time = new Date();

    datetext = time.toTimeString();
    time = datetext.split(' ')[0];

    return time;
}


/**
 * Функция добавления индикатора набора, если собеседник набираем сообщение
 */
function add_typing_indicator() {

    if($(".typing").length == 0 ) {

        var row = document.createElement("div");
        $(row).addClass("col-lg-12 col-md-12 col-sm-12 col-xs-12 typing");

        var col12 = document.createElement("div");
        $(col12).addClass("col-lg-12 col-md-12 col-sm-12 col-xs-12 text-left");

        $(col12).html("<i class='fa fa-pencil'></i> пишет...");

        $(row).append($(col12));
        $("#messages").append($(row));
        
        setTimeout(function(){
            $(row).remove();
        }, 1000);

    }

}


/**
 * Открываем форму со стикерами по нажатию кнопки
 */
$('.attach_form').click(function() {

    if($('.attach').css("display") == "block") {
        $('.attach').fadeOut("fast");
    }

    else {
        $('.attach').fadeIn("fast");
    }

});


/**
 * Отправляем стикер по нажатию по нему
 */
$('.sticker').click(function() {

    var sticker_id = $(this).data("sticker");
    socket.emit("send sticker", {"sticker_id": sticker_id});
    $('.attach').css("display", "none");

});


/**
 * Отправка сообщения 
 */
$('#msgForm').submit(function() {

    var text = $('#m').val();

    if(text != '') {
        socket.emit( 'chat message', $('#m').val() );
        $('#m').val('');
    }
        
    return false;

});


/**
 * Говорим серверу о том, что пользователь набираем сообщение
 */
$('#m').on("input", function() {
    socket.emit("typing text", '' );
});


$("#sendPhoto").on("click", function() {
    $('#photo').trigger('click');
});

$('#photo').on("change", function() {
    $("#sendPhotoForm").submit();
});


$("#sendPhotoForm").on("submit", function() {

    $.ajax({
        url: "/sendPhoto",
        data: new FormData( this ),
        method: "POST",
        cache: false,
        contentType: false,
        processData: false,

        success: function(data) {
            socket.emit("chat message", data);
        }
    });

    return false;

});

/* Убираем интервал если вкладка была выбрана */
$(window).focus(function() {

    clearInterval(intervalTitle);

});

/**
 * Функция для изменения названия вкладке при наличии новых событий
 */

function changeTabTitle() {

    var title = "Анонимный чат";
    var new_title = "Новое сообщение";
    


    if( activeTab == 0 ) {
        intervalTitle = setInterval(function() {

            if( document.title == title ) {
                document.title = new_title;
            }

            else {
                document.title = title;
            }

        }, 1000)
    }
    else {

    }
}


/* Отключаем пользовательские элементы */
function disableInterface() {

    $("#m").prop("disabled", true);
    $("#sendMsg").prop("disabled", true);
    $("#sendPhoto").prop("disabled", true);

    $('.attach_form').hide();

}


/* Включаем пользовательские элементы */
function enableInterface() {

    $("#m").prop("disabled", false);
    $("#sendMsg").prop("disabled", false);
    $("#sendPhoto").prop("disabled", false);

    $('.attach_form').show();

}