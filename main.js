var express = require('express');
var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
var multer  = require('multer');
var url = require('url');


var storage =   multer.diskStorage({

  destination: function (req, file, callback) {
    callback(null, './public/uploads');
  },

  filename: function (req, file, callback) {
    callback(null, Date.now() + file.originalname);
  }

});

var upload = multer({ storage : storage}).single('photo');


app.use(express.static('public')); // Указываем, что в public лежат наши статические файлы


app.get('/', function(req, res){
	res.sendFile('index.html', { root: __dirname });
});


app.get('/chat', function(req, res){
	res.sendFile('chat.html', { root: __dirname });
});


app.get('/about', function(req, res){
	res.sendFile('about.html', { root: __dirname });
});


app.post('/sendPhoto', function(req, res) {

    upload(req,res,function(err) {

        if(err) {
            return res.end("Error uploading file.");
        }
        var url = "http://" + req.headers.host + "/uploads/" + req.file.filename;
        res.end(url);

    });

});

var users = [];
var rooms = [];
var waiters = [];

io.on('connection', function(socket) {
	users[socket.id] = socket;


	// Если число ожидающих чата больше нуля, то
	// организовывает чат между новым пользователем и первым ожидающим
	socket.on("start chat", function(params) {

		// Фильтруем собеседников по полам
		var filterWaiters = waiters.filter(function(obj) {

			var waiterGender = obj.params.myGender;
			var waiterChatGender = obj.params.chatGender;
			var myGender = params.myGender;
			var myChatGender = params.chatGender;

			if( waiterChatGender == "n" && myChatGender == waiterGender ) {
				return obj;
			}

			if( waiterChatGender == myGender && myChatGender  == "n" ) {
				return obj;
			}

			if( waiterChatGender == "n" && myChatGender == "n" ) {
				return obj;
			}

			if( myGender == waiterChatGender && myChatGender == waiterGender ) {
				return obj;
			}





		});

		// Если есть собеседники с такими фильтрами
		if(filterWaiters.length > 0) {

			rooms[filterWaiters[0].socket_id] = socket.id;
			rooms[socket.id] = filterWaiters[0].socket_id;

			socket.emit( "server message", "connect" );
			users[filterWaiters[0].socket_id].emit( "server message", "connect");

			var index = waiters.indexOf(filterWaiters[0]);
			waiters = waiters.splice(1, index);
		
		}

		// Иначе добавляем их в пул к ожидающим
		else {


			// Если пользователь не указал по каким-то причинам(пытается сломать чат) параметры поиска,
			// то он, вероятнее всего, педик!
			if(params.chatGender == undefined) {
				params.chatGender == "f";
			}

			if(params.myGender == undefined) {
				params.myGender == "f";
			}


			var waiter = {
				"socket_id" : socket.id,
				"params": params 
			}

			socket.emit( "server message", "wait" );
			waiters.push(waiter);
		}

		
	});


	// Отдаем кол-во пользователей на сайте
	socket.on("count users", function() {
		socket.emit( "count users", io.engine.clientsCount );
	});


	// Отправка сообщения пользователям
	socket.on("chat message", function(msg) {

		try {

			u1 = users[socket.id];
			u2 = users[rooms[socket.id]];

			u1.emit( "chat message", { "sender": 0, "text": msg } );
			u2.emit( "chat message", {"sender": 1, "text": msg } );

		}

		catch(e) {
			u1.emit( "server message", "Connection lost" );
		}

	});


	// Отправка стикеров
	socket.on("send sticker", function(sticker) {

		try {

			u1 = users[socket.id];
			u2 = users[rooms[socket.id]];

			u1.emit( "send sticker", { "sender": 0, "sticker_id": sticker.sticker_id } );
			u2.emit( "send sticker", { "sender": 1, "sticker_id": sticker.sticker_id } );

		}

		catch(e) {
			u1.emit( "server message", "Connection lost" );
		}

	});


	// Отправка команды, что пользоватеь набирает сообщение
	socket.on("typing text", function() {

		try {

			u2 = users[rooms[socket.id]];
			u2.emit( "typing text", '' );

		}

		catch(e) {
			socket.emit( "server message", "Connection lost" );
		}

	});


	// Отключение пользователя
	socket.on('disconnect', function() {

		try {

			user_id = rooms[socket.id];
			user = users[user_id];

			// Если пользователь с таким ID в пуле с ожидающими, то удаляем его
			var waiter = waiters.filter(function(obj) {

				if( obj.socket_id == socket.id ) {
					return obj;
				}

			});

			var index = waiters.indexOf(waiter[0]);

			if(index != -1) {
				waiters = waiters.splice(1, index);
			}

			if(user != undefined) {
				user.emit( "server message", 'user disconnect' );
			}

			delete users[socket.id];
		} 

		catch(e) {}
	});
});


http.listen(3000, function() {
	console.log('listening on *:3000');
});

